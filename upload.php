<!DOCTYPE html>
<html>
<head></head>

<body>
<?php

include('login_info.php');
$connection = mysqli_connect($HOST, $USER, $PASSWORD, $DATABASE, $PORT)
or die('Login error');

if(isset($_POST['upload'])&&$_FILES['f']['size']>0 &&$_FILES['f']['size']<4194304){
	$tmpr_name = $_FILES['f']['tmp_name'];
	$file_name = addslashes($_FILES['f']['name']);
	$data_type = $_FILES['f']['type'];
	$file_size = $_FILES['f']['size'];
	
	$fd = fopen($tmpr_name, 'r');
	$content = fread($fd, filesize($tmpr_name));
	fclose($fd);
	if(strpos(str_replace("\'",""," $file_name"),"'")!=false){
        addslashes($file_name);
	}

	$query = "INSERT INTO files(file_name, file_type, file_size, content)VALUES(?, ?, ?, ?)";
	
	$stmt = mysqli_prepare($connection, $query);
	mysqli_stmt_bind_param($stmt, "ssib", $file_name, $data_type, $file_size, $content);
	mysqli_stmt_execute($stmt);
	
	$affected_rows = mysqli_stmt_affected_rows($stmt);
	if ($affected_rows ==1){
		echo 'DATA CREATED';
		mysqli_stmt_close($stmt);
		mysqli_close($connection);
	}
	else{
	echo 'ERROR';
	echo mysqli_error();
	}
}

?>
</body>
</html>